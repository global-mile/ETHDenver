
import { UsersActor, users } from './users';
import { ParcelsActor, parcels } from './parcels';

export type Actors = {
    users: UsersActor,
    parcels: ParcelsActor,
}

export const canisters = {
    users,
    parcels,
};

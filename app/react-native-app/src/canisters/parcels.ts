import { Canister } from "@bundly/ic-core-js";
import { ActorSubclass } from "@dfinity/agent";

import { _SERVICE } from "../declarations/parcels/parcels.did";
// @ts-ignore
import { idlFactory } from "../declarations/parcels/parcels.did.js";

export type ParcelsActor = ActorSubclass<_SERVICE>;

export const parcels: Canister = {
    idlFactory: idlFactory  as any,
    configuration: {
        canisterId: 'be2us-64aaa-aaaaa-qaabq-cai'
    }
}

import { Canister } from "@bundly/ic-core-js";
import { ActorSubclass } from "@dfinity/agent";

import { _SERVICE } from "../declarations/users/users.did";
// @ts-ignore
import { idlFactory } from "../declarations/users/users.did.js";

export type UsersActor = ActorSubclass<_SERVICE>;

export const users: Canister = {
    idlFactory: idlFactory  as any,
    configuration: {
        canisterId: 'br5f7-7uaaa-aaaaa-qaaca-cai'
    }
}

import React from 'react';
import { ActivityIndicator } from 'react-native';
import { View, Text, Colors } from 'react-native-ui-lib';

const Container = ({
  children,
  safeArea = false,
  loading = false,
  ...rest
}) => {
  return (
    <>
      {loading && (
      <View style={{ position: 'absolute', width: '100%', height: '100%', backgroundColor: 'black', zIndex: 999, flex: 1, justifyContent: 'center', alignItems: 'center', opacity: .7 }}>
        <ActivityIndicator size="large" color={Colors.$textPrimary}  />
        <Text color='white' text40BO>Cargando</Text>
      </View>
      )}
      <View useSafeArea={safeArea} paddingH-20 flex {...rest}>
        {children}
      </View>
    </>
  );
};

export default Container;
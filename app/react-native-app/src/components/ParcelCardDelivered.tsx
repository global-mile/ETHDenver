import React from 'react';
import { View, Text, Card } from 'react-native-ui-lib';
import { Parcel } from '../services/useParcelCanisterService'
import GMButton from './GMButton';

type ParcelCardDeliveredProps = {
  parcel: Parcel
}

const ParcelCardDelivered = ({ parcel} : ParcelCardDeliveredProps) => {
  
  return (
    <Card backgroundColor='white' marginV-10 padding-20 borderRadius={0} >
      <Text text70BO>{parcel.name}</Text>
      <Text>
        <Text text70BO>Pago: </Text> 
        <Text text70L>${parcel.price} MXN</Text>      
      </Text>
    </Card>
  );
};

export default ParcelCardDelivered;
import React from 'react';
import { View, Text, Card } from 'react-native-ui-lib';
import { Parcel } from '../services/useParcelCanisterService'
import GMButton from './GMButton';

type StateCardProps = {
  title: string;
  value: string;
  marginRight?: boolean;
  marginLeft?: boolean;
}

const StatCard = ({title, value, marginLeft, marginRight} : StateCardProps) => {
  return (
    <Card center marginL-5={marginLeft} marginR-5={marginRight} flex backgroundColor='white' marginV-10 padding-20 borderRadius={0} >
      <Text text70BO>{title}</Text>
      <Text text30BO>{value}</Text>
    </Card>
  );
};

export default StatCard;
import { useAuth } from "@bundly/ic-react";
import { Stack } from "expo-router/stack";
import { Tabs } from 'expo-router/tabs'
import { Link } from 'expo-router';

export default function Layout() {

  const { isAuthenticated } = useAuth()

  return <>
    <Stack screenOptions={{
      // Hide the header for all other routes.
      headerShown: false,
    }} 
    />
  </>
}

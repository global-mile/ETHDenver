import { Tabs } from 'expo-router';
import Ionicons from '@expo/vector-icons/Ionicons';

export default function HomeLayout() {
    return (
      <Tabs>
        <Tabs.Screen
          // Name of the route to hide.
          name="parcels"
          options={{
            href: '/home/parcels',
            title: 'Paquetes',
            tabBarIcon: () => (<Ionicons name="cube-outline" size={25} />),
            tabBarLabelStyle: { fontSize: 15, fontWeight: 'bold' }
          }}
        />
        <Tabs.Screen
          name="delivers"
          options={{
            href: '/home/delivers',
            title: 'Entregas',
            tabBarIcon: () => (<Ionicons name="checkbox-outline" size={25} />),
            tabBarLabelStyle: { fontSize: 15, fontWeight: 'bold' }
          }}
        />
        <Tabs.Screen
          name="earnings"
          options={{
            href: '/home/earnings',
            title: 'Ganancias',
            tabBarIcon: () => (<Ionicons name="cash-outline" size={25} />),
            tabBarLabelStyle: { fontSize: 15, fontWeight: 'bold' }
          }}
        />
        <Tabs.Screen
          name="account"
          options={{
            href: '/home/account',
            title: 'Cuenta',
            tabBarIcon: () => (<Ionicons name="person-circle-outline" size={25} />),
            tabBarLabelStyle: { fontSize: 15, fontWeight: 'bold' }
          }}
        />
      </Tabs>
    );
  }
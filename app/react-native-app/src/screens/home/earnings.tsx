import { useCallback, useEffect, useState } from "react";
import useParcelCanisterService, { Parcel } from "../../services/useParcelCanisterService";
import { useFocusEffect } from "expo-router";
import Container from "../../components/Container";
import { FlatList } from "react-native";
import ParcelCard from "../../components/ParcelCard";
import ParcelCardDelivered from "../../components/ParcelCardDelivered";
import { View, Card } from 'react-native-ui-lib'
import StatCard from "../../components/StatCard";

export default function EarningScreens() {
    const { getMyParcels, setParcelStatus, getMyEarnings } = useParcelCanisterService();  

    const [deliveredParcels, setDeliveredParcels] = useState<Parcel[]>([])
    const [earning, setEaring] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(true);

    const getMyParcelsLocal = async () => {
        const parcels = await getMyParcels();
        setDeliveredParcels(parcels.filter((parcel) => parcel.status === 'delivered'))
    } 

    const getMyEarningsLocal = async () => {
        const localEarnings = await getMyEarnings()
        if (localEarnings) {
            setEaring('$' + localEarnings)
        }
    }

    useFocusEffect(
        useCallback(() => {
          const fetchData = async () => {
            // Fetch data here
            setLoading(true);
            await getMyEarningsLocal()
            await getMyParcelsLocal()
            setLoading(false);
          };
    
          fetchData();
    
          return () => {
            // Clean up here
          };
        }, [])
      );

    return (
    <Container paddingT-20 loading={loading}>
        <View row>
            <StatCard title="Ganancias" value={earning} marginRight />
            <StatCard title="Entregas" value={deliveredParcels.length.toString()} marginLeft/>
        </View>
        <FlatList 
            data={deliveredParcels}
            renderItem={({ item }) => (
                <ParcelCardDelivered parcel={item as Parcel} />
            )}
        />
    </Container>)
}
import { FlatList } from "react-native"
import Container from "../../components/Container"
import useParcelCanisterService, { Parcel } from "../../services/useParcelCanisterService";
import { useCallback, useEffect, useState } from "react";
import ParcelCard from "../../components/ParcelCard";
import { useFocusEffect } from "expo-router";
import { Card, Text } from 'react-native-ui-lib'

export default function Parcels() {
    const { getParcelsByStatus, asignParcelToDriver } = useParcelCanisterService();  
    const [pendingParcels, setPendingParcels] = useState<Parcel[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    const getParcelsByStatusLocal = async () => {
        const parcels = await getParcelsByStatus('pending')
        setPendingParcels([...parcels])
    }

    const fetchData = async () => {
      // Fetch data here
      setLoading(true);
      setTimeout(async () => {
        await getParcelsByStatusLocal()
        setLoading(false);
      }, 1000)
    };

    useFocusEffect(
        useCallback(() => {    
          fetchData();
    
          return () => {
            // Clean up here
          };
        }, [])
      );

    const handleConfirmParcel = async (parcel: Parcel) => {
        setLoading(true)
        try {
          await asignParcelToDriver(parcel.id.toString());
        } catch(e) {
          console.log({e})
        }
        fetchData();
    }

    return (
    <Container paddingT-20 loading={loading}>
        <Text center marginB-20>
          <Text text60BL>Confirmaciones pendientes: {pendingParcels.length}</Text>          
        </Text>
        <FlatList 
            data={pendingParcels}
            renderItem={({ item }) => (
                <ParcelCard borderColor={'#50C878'} confirmTitle="Confirmar" parcel={item as Parcel} onConfirm={() => handleConfirmParcel(item)} />
            )}
            ListEmptyComponent={() => (
            <Card center borderRadius={0} padding-35 marginT-30>
              <Text text30BO center>No hay paquetes disponibles</Text>
            </Card>)}
        />
    </Container>
    )
}
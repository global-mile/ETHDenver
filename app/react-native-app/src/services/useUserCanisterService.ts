'use client';
import { useActor } from '@bundly/ic-react';
import { Actors } from '../canisters';
import type { Principal } from '@dfinity/principal';

export type User = {
    id: Principal;
    client: string;
    username: string;
}

const useUserCanisterService = () => {
    const usersActor = useActor<Actors>('users') as Actors["users"];

    const getMe = async () => {
        try {
            const me = await usersActor.getUser();
            
        } catch (error) {
            console.error(error);
            
        }
    }

    const createUser = async (username: string, client: string) => usersActor.createNewUser(username, client);

    return {
        getMe,
        createUser,
    }
};

export default useUserCanisterService;

import React from 'react';
import { View } from 'react-native-ui-lib';

const Container = ({
  children,
  safeArea = false,
  ...rest
}) => {
  return (
    <View useSafeArea={safeArea} paddingH-20 flex {...rest}>
      {children}
    </View>
  );
};

export default Container;
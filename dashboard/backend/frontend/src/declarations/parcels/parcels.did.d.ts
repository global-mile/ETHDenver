import type { Principal } from '@dfinity/principal';
import type { ActorMethod } from '@dfinity/agent';
import type { IDL } from '@dfinity/candid';

export interface _SERVICE {
  'assignParcelToDriver' : ActorMethod<[string], boolean>,
  'createNewParcel' : ActorMethod<
    [string, string, string, string, string, string, string, string],
    boolean
  >,
  'getAllParcels' : ActorMethod<
    [],
    Array<
      {
        'id' : Principal,
        'lat' : string,
        'lng' : string,
        'status' : string,
        'client' : string,
        'name' : string,
        'phone_receiver' : string,
        'user_id' : Principal,
        'driver_id' : [] | [Principal],
        'address' : string,
        'price' : string,
        'name_receiver' : string,
      }
    >
  >,
  'getMyEarnings' : ActorMethod<[], string>,
  'getMyParcels' : ActorMethod<
    [],
    Array<
      {
        'id' : Principal,
        'lat' : string,
        'lng' : string,
        'status' : string,
        'client' : string,
        'name' : string,
        'phone_receiver' : string,
        'user_id' : Principal,
        'driver_id' : [] | [Principal],
        'address' : string,
        'price' : string,
        'name_receiver' : string,
      }
    >
  >,
  'getParcelByStatus' : ActorMethod<
    [string],
    Array<
      {
        'id' : Principal,
        'lat' : string,
        'lng' : string,
        'status' : string,
        'client' : string,
        'name' : string,
        'phone_receiver' : string,
        'user_id' : Principal,
        'driver_id' : [] | [Principal],
        'address' : string,
        'price' : string,
        'name_receiver' : string,
      }
    >
  >,
  'getParcelByStatusByDriver' : ActorMethod<
    [string],
    Array<
      {
        'id' : Principal,
        'lat' : string,
        'lng' : string,
        'status' : string,
        'client' : string,
        'name' : string,
        'phone_receiver' : string,
        'user_id' : Principal,
        'driver_id' : [] | [Principal],
        'address' : string,
        'price' : string,
        'name_receiver' : string,
      }
    >
  >,
  'getUserParcels' : ActorMethod<
    [],
    Array<
      {
        'id' : Principal,
        'lat' : string,
        'lng' : string,
        'status' : string,
        'client' : string,
        'name' : string,
        'phone_receiver' : string,
        'user_id' : Principal,
        'driver_id' : [] | [Principal],
        'address' : string,
        'price' : string,
        'name_receiver' : string,
      }
    >
  >,
  'setParcelStatus' : ActorMethod<[string, string], boolean>,
}
export declare const idlFactory: IDL.InterfaceFactory;
export declare const init: ({ IDL }: { IDL: IDL }) => IDL.Type[];

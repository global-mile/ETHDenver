export const idlFactory = ({ IDL }) => {
  return IDL.Service({
    'createNewUser' : IDL.Func([IDL.Text, IDL.Text], [IDL.Principal], []),
    'getAllUsers' : IDL.Func(
        [],
        [
          IDL.Vec(
            IDL.Record({
              'id' : IDL.Principal,
              'client' : IDL.Text,
              'username' : IDL.Text,
            })
          ),
        ],
        ['query'],
      ),
    'getUser' : IDL.Func(
        [],
        [
          IDL.Record({
            'id' : IDL.Principal,
            'client' : IDL.Text,
            'username' : IDL.Text,
          }),
        ],
        ['query'],
      ),
  });
};
export const init = ({ IDL }) => { return []; };

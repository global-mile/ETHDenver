import type { Principal } from '@dfinity/principal';
import type { ActorMethod } from '@dfinity/agent';
import type { IDL } from '@dfinity/candid';

export interface _SERVICE {
  'createNewUser' : ActorMethod<[string, string], Principal>,
  'getAllUsers' : ActorMethod<
    [],
    Array<{ 'id' : Principal, 'client' : string, 'username' : string }>
  >,
  'getUser' : ActorMethod<
    [],
    { 'id' : Principal, 'client' : string, 'username' : string }
  >,
}
export declare const idlFactory: IDL.InterfaceFactory;
export declare const init: ({ IDL }: { IDL: IDL }) => IDL.Type[];

"use client";

import UserList from "@app/components/user-list";
import CreateUserForm from "@app/components/create-user-form";
import { redirect } from 'next/navigation'

export default function Home() {


  return (
    <>
      <main className="h-full w-full">
        <Content />
      </main>
    </>
  );
}

function Content() {
  redirect('/dashboard');

  return <></>
}

function PrivateContent() {
  return <>
    <section className="h-1/2 flex">
      <div className="w-1/2 h-full flex items-center justify-center">
        <CreateUserForm />
      </div>
      <div className="w-1/2 h-full flex items-center justify-center">
        <UserList />
      </div>
    </section>
    <section className="h-1/2 flex">
      <div className="w-1/2 h-full flex items-center justify-center">
      </div>
      <div className="w-1/2 h-full flex items-center justify-center">
      </div>
    </section>
  </>
}

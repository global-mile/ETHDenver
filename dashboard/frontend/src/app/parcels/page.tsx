"use client";
import Table from "@app/components/table/table";
import useParcelCanisterService, { ParcelCanisterType } from "@app/services/useParcelCanisterService";
import useUserCanisterService from "@app/services/useUserCanisterService";
import { useEffect } from "react";
import type { Principal } from '@dfinity/principal';
import { RootState } from "@app/redux/store";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "@app/redux/slices/app";

export type Parcel = {
  id: Principal;
  name: string;
  lat: string;
  lng: string;
  client: string;
  address: string;
  nameReceiver: string;
  phoneReceiver: string;
  status: string;
  price: number;
  userId?: Principal;
  driverId?: [] | [Principal]
};


export default function Parcels() {
  const { createParcel, getMyParcels } = useParcelCanisterService();

  const { myParcels } = useSelector((state: RootState) => state.user);
  const dispatch = useDispatch();

    const columns = [
      'Nombre',
      'Latitud',
      'Longitud',
      'Dirección',
      'Nombre del receptor',
      'Teléfono del receptor',
      'Estado',
      'Precio', 
    ];

    const columnsToRender = [
      'name',
      'lat',
      'lng',
      'status',
      'address',
      'nameReceiver',
      'phoneReceiver',
      'price',
    ];

    const createFields = [
      { name: 'name', label: 'Nombre', type: 'text', value: 'Joyeria' },
      { name: 'lat', label: 'Latitud', type: 'text', value: '20.6752'  },
      { name: 'lng', label: 'Longitud', type: 'text', value: '-103.3473' },
      { name: 'client', label: 'Cliente', type: 'text', value: 'CAINIAO'},
      { name: 'address', label: 'Dirección', type: 'text', value: 'San juan de dios 3015' },
      { name: 'nameReceiver', label: 'Nombre del receptor', type: 'text', value: 'Pedro Alvarez'},
      { name: 'phoneReceiver', label: 'Teléfono del receptor', type: 'text', value: '3313141815'},
      { name: 'price', label: 'Precio', type: 'number', value: 30},
    ];

    const handleCreateParcel = async (parcel: Parcel) => {
      dispatch(setLoading(true));
      await createParcel(parcel);
      await getMyParcels();
      dispatch(setLoading(false));
    }
    
    return (
      <>
      {myParcels && (
      <Table columnsToRender={columnsToRender} title="Envios" data={myParcels} columns={columns} createFields={createFields} onAddItem={(item) => handleCreateParcel(item)} />
      )}
    </>
    )
  }
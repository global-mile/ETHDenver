"use client";
import { RootState } from '@app/redux/store';
import LoadingOverlay from 'react-loading-overlay-ts';
import { useSelector } from 'react-redux';

const Loading = () => {
  const { loading } = useSelector((state: RootState) => state.app);

  return (
    <LoadingOverlay
      active={loading}
      spinner
      text='Cargando...'
    />
  )
}

export default Loading;
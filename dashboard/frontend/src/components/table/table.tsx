"use client";
import { Dialog, Transition } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/24/outline";
import { Fragment, useEffect, useState } from "react";

type Field = {
  name: string;
  label: string;
  type: string;
  value?: any;
}

interface TableProps {
    title: string;
    data: Record<string, any>[];
    columns: string[];
    columnsToRender: string[];
    createFields: Field[];
    onAddItem?: (item: any) => void;
}

const Table = ({ title, data, columns, createFields, columnsToRender, onAddItem }: TableProps) => {

  const [addItemModalOpen, setAddItemModalOpen] = useState(false);

  const [itemToAdd, setItemToAdd] = useState<Record<string, any>>({});

  useEffect(() => {
    setItemToAdd({
      ...createFields.reduce((acc, field) => ({ ...acc, [field.name]: field.value }), {})
    });
  }, []);

  const handleFormSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setAddItemModalOpen(false);

    onAddItem?.(itemToAdd);
  }

  const onChangeItemValue = (name: string, value: string) => {
    setItemToAdd({ ...itemToAdd, [name]: value });
  }

    return (
      <>
        <div className="px-4 sm:px-6 lg:px-8">
      <div className="sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-base font-semibold leading-6 text-gray-900">{title}</h1>
        </div>
        <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
          <button
            type="button"
            className="block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            onClick={() => setAddItemModalOpen(true)}
          >
            Add
          </button>
        </div>
      </div>
      <div className="mt-8 flow-root">
        <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <table className="min-w-full divide-y divide-gray-300">
              <thead>
                <tr>
                    {columns.map((column) => (
                        <th key={`column-${column}`} scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">
                            {column}
                        </th>
                    ))}
                  <th key={''} scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">
                    Acciones
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200">
                {data.map((row, index) =>(
                  <tr key={`row-${index}`}>
                    {Object.keys(row).filter((column) => columnsToRender.includes(column)).map((value, i) => (
                        <td key={`field-${i}`} className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{row[value]}</td>
                    ))}
                    <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-0">
                      <a href="#" className="text-indigo-600 hover:text-indigo-900">
                        Editar<span className="sr-only">, {row[index]}</span>
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <Transition.Root show={addItemModalOpen} as={Fragment}>
      <Dialog style={{ zIndex: 999 }} as="div" className="relative z-10" onClose={setAddItemModalOpen}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-sm sm:p-6">
              <form className="space-y-6" onSubmit={handleFormSubmit}>
                {createFields.map((field) => (
                    <div key={`create-input-${field.name}`}>
                       <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                         {field.label}
                       </label>
                       <div className="mt-2">
                         <input
                           onChange={(e) => onChangeItemValue(field.name, e.target.value)}
                           id={field.name}
                           name={field.name}
                           type={field.type}
                           value={itemToAdd[field.name]}
                           required
                           className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                         />
                       </div>
                     </div>
                     
                    ))}
                    <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Enviar
              </button>
            </div>
                </form>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
    </>
    )
};

export default Table;
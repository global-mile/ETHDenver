import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  loading: false,
}

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setLoading: (state, action) => {
        state.loading = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { setLoading } = appSlice.actions

export default appSlice.reducer
'use client';
import { useActor } from '@bundly/ic-react';
import { Actors } from '../canisters';
import type { Principal } from '@dfinity/principal';
import { useDispatch } from 'react-redux';
import { setMe } from '@app/redux/slices/user';

export type User = {
    id: Principal;
    client: string;
    username: string;
}

const useUserCanisterService = () => {
    const usersActor = useActor<Actors>('users') as Actors["users"];
    const dispatch = useDispatch();

    const getMe = async () => {
        try {
            const me = await usersActor.getUser();
            dispatch(setMe(me));
        } catch (error) {
            console.error(error);
            dispatch(setMe(null));
        }
    }

    const createUser = async (username: string, client: string) => usersActor.createNewUser(username, client);

    return {
        getMe,
        createUser,
    }
};

export default useUserCanisterService;
